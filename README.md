## Usage

Be sure to have [Node.js](https://nodejs.org/) installed before proceeding.

```shell
# Clone the repo
git clone https://mcediego@bitbucket.org/mcediego/cucinellipoc.git
cd cucinellipoc

# Install dependencies
npm install

# Run the App
node bin/www

# Browser
open the browser at: http://localhost:3000/
```