const Config = {
  languages: {
    active: ['en','it'],
    main: 'en'
  }
};

module.exports = Config;
