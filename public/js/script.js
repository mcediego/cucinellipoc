$(document).on("scroll", function(){

  //Logo Shrink Animation
  if ($(document).scrollTop() < 200){
    $(".row-logo").removeClass("shrink");
  } else if ($(document).scrollTop() > 400){
    $(".row-logo").addClass("shrink");
  }

});

$(function() {
  //Redirect on language selection
  $("#langConfirm").click(function () {
    window.location.replace( $("#langSelect").val() );
  });
});