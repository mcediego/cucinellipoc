var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//Custom modules
var i18n = require('i18n');
var sassMiddleware = require('node-sass-middleware');
var languageEngine = require('./lib/language-engine');
var appConfig = require('./config');

//Routing loading
var indexRouter         = require('./routes/index');
var langHandlerRouter   = require('./routes/lang-handler');
var registrationLanding = require('./routes/registration-landing');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Application Routing definition
app.use('/',                       indexRouter);
app.use('/:lang/*',                langHandlerRouter);
app.use('/*/registration-landing', registrationLanding);

//Frontend Routing definition
app.use('/js',  express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/js',  express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/js',  express.static(__dirname + '/node_modules/\@fortawesome/fontawesome-free/js')); // redirect JS fontawesome
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap

//SASS compiler
//app.use(
    //sassMiddleware({
        //src: __dirname +  '/public/sass', //where the sass files are 
        //dest: __dirname + '/public/css', //where css should go
//debug: true,
  //outputStyle: 'compressed'
    //})
//);
//express.static(path.join(__dirname, 'public'));

//i18n Configuration init
i18n.configure({
  // setup some locales - other locales default to en silently
  locales: appConfig.languages.active,
  default: appConfig.main,
  // sets a custom cookie name to parse locale settings from
  //cookie: 'yourcookiename',
  // where to store json files - defaults to './locales'
  directory: __dirname + '/locales'
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
