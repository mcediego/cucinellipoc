var express        = require('express');
var router         = express.Router();
var languageEngine = require('../lib/language-engine');
const i18n         = require('i18n');
var appConfig      = require('../config');
var db             = require('../lib/dummy-data');

/* GET home page. */
router.get('/', function(req, res, next) {

  //languageEngine.sayHi();
  console.log(languageEngine.urlRemoveLang(req.originalUrl));
  console.log(req.originalUrl);

  res.render('index', { 
    title: 'Welcome', 
    i18n: i18n, 
    appConfig: appConfig,
    urlWithoutLang: languageEngine.urlRemoveLang(req.originalUrl),
    originalUrl: req.originalUrl,
    mainMenu: db.mainMenu
   });
});

module.exports = router;
