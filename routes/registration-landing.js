var express        = require('express');
var router         = express.Router();
var languageEngine = require('../lib/language-engine');
var db             = require('../lib/dummy-data');
var languageEngine = require('../lib/language-engine');
const i18n         = require('i18n');
var appConfig      = require('../config');

/* GET Lading */
router.get('/', function(req, res, next) {

  console.log('Cur language');
  console.log(i18n.getLocale());
  res.render('registration-landing', {
    title: 'Landing',
    mainMenu: db.mainMenu,
    i18n: i18n,
    appConfig: appConfig,
    urlWithoutLang: languageEngine.urlRemoveLang(req.originalUrl),
    originalUrl: req.originalUrl,
    curUser: db.curUser,
    sections: db.landing.sections,
  });
});

module.exports = router;