var express        = require('express');
var router         = express.Router({ mergeParams: true });
var languageEngine = require('../lib/language-engine');
const i18n         = require('i18n');


/* Main language Route */
router.get('/', function(req, res, next) {

  if(req.params.lang !== 'css' && req.params.lang !== 'js') {
    //Detect valid enabled language from url
    var langFound = languageEngine.detectLanugage(req.params.lang);

    //If found set the appropriate language
    if(langFound !== false) {
      i18n.setLocale(langFound);
    }
  }

  //Go to the next route
  next();
});

module.exports = router;