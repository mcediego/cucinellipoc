const DummyData = {
  mainMenu: [
    'Sales',
    'Man',
    'Woman',
    'Children',
    'LifeStyle',
    'Stories'
  ],
  curUser: {
    firstName: "Francesco"
  },
  landing: {
    sections: [
      {
        col1: {
          icon1:  'fa-shopping-bag',
          icon2:  '',
          lang: {
            en: {
              title: 'My orders',
              body:  'Track shipments, exchange or return items that are not for you',
            },
            it: {
              title: 'I miei ordini',
              body:  'Segui le spedizioni, cambia o rendi gli articoli che non fanno per te',
            }
          }
        },
        col2: {
          icon1:  'fa-headset',
          icon2:  '',
          lang: {
            en: {
              title: 'Support',
              body:  'Any problem? we are here to help you out!',
            },
            it: {
              title: 'Supporto',
              body:  'Bisogno di supporto? Siamo qui a tua disposizione!',
            }
          }
        },
      },
      {
        col1: {
          icon1:  'fa-heart',
          icon2:  '',
          lang: {
            en: {
              title: 'Dream Box',
              body:  'Keep an eye on the items you want most and don\'t miss them',
            },
            it: {
              title: 'Dream Box',
              body:  'Tieni d’occhio gli articoli che più desideri e non lasciarteli scappare',
            }
          }
        },
        col2: {
          icon1:  'fa-user',
          icon2:  '',
          lang: {
            en: {
              title: 'My Data',
              body:  'Change your registration details, your addresses and payment methods',
            },
            it: {
              title: 'I Miei Dati',
              body:  'Modifica i tuoi dati di registrazione, i tuoi indirizzi e i metodi di pagamento',
            }
          }
        },
      },
    ],
    exSections: [
      {
        col1: {
          icon1:  'fa-shopping-bag',
          icon2:  '',
          lang: {
            en: {
              title: 'My orders',
              body:  'Track shipments, exchange or return items that are not for you',
            },
            it: {
              title: 'I miei ordini',
              body:  'Segui le spedizioni, cambia o rendi gli articoli che non fanno per te',
            }
          }
        },
        col2: {
          icon1:  'fa-headset',
          icon2:  '',
          lang: {
            en: {
              title: 'Support',
              body:  'Any problem? we are here to help you out!',
            },
            it: {
              title: 'Supporto',
              body:  'Bisogno di supporto? Siamo qui a tua disposizione!',
            }
          }
        },
      },
      {
        col1: {
          icon1:  'fa-heart',
          icon2:  '',
          lang: {
            en: {
              title: 'Dream Box',
              body:  'Keep an eye on the items you want most and don\'t miss them',
            },
            it: {
              title: 'Dream Box',
              body:  'Tieni d’occhio gli articoli che più desideri e non lasciarteli scappare',
            }
          }
        },
        col2: {
          icon1:  'fa-user',
          icon2:  '',
          lang: {
            en: {
              title: 'My Data',
              body:  'Change your registration details, your addresses and payment methods',
            },
            it: {
              title: 'I Miei Dati',
              body:  'Modifica i tuoi dati di registrazione, i tuoi indirizzi e i metodi di pagamento',
            }
          }
        },
      },
    ]
  }
};

module.exports = DummyData;