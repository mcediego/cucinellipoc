/*
 * Language Handler
 * @author Diego Goretti
 */
var appConfig = require('./../config');
const i18n    = require('i18n');


class LanguageEngine {

  //Detect current language from URL
  detectLanugage(curLang) {
    var langFound   = false;
    var enabledLang = appConfig.languages.active;

    for(var x in enabledLang) {
      if(enabledLang[x] === curLang) {
        langFound = curLang;
      }
    }

    return langFound;
  }

  urlRemoveLang(url) {
    var out = url.split("/");
    return out.slice(2).join("/");
  }
}

module.exports = new LanguageEngine();
